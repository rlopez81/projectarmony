package com.example.projecta

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val mail:String = intent.extras?.getString("temail").orEmpty()

        setup(mail)
    }
    private fun setup(email:String){
        val temail = findViewById<TextView>(R.id.email)
        temail.text = email
        val blogout = findViewById<Button>(R.id.logout)
        blogout.setOnClickListener{
            FirebaseAuth.getInstance().signOut()
            onBackPressed()
        }


    }
}